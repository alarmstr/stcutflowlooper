#include "STCutflowLooper/Looper.h"
#include "STCutflowLooper/Config.h"
#include "PathResolver/PathResolver.h"
#include "xAODRootAccess/tools/ReturnCheck.h"
// RETURN_CHECK can only be called in functions returning bool or TReturnCode

//std/stl
#include <iostream>
using std::cout;
using std::endl;
#include <iomanip>
#include <sstream>
using std::stringstream;
#include <fstream>

// xAOD Decorators
const static SG::AuxElement::Decorator<int> dec_charge("MyCharge");

// xAOD Accessors
const static SG::AuxElement::ConstAccessor<char> acc_bad("bad");
const static SG::AuxElement::ConstAccessor<char> acc_cosmic("cosmic");
const static SG::AuxElement::ConstAccessor<char> acc_baseline("baseline");
const static SG::AuxElement::ConstAccessor<char> acc_signal("signal");
const static SG::AuxElement::ConstAccessor<char> acc_passOR("passOR");
const static SG::AuxElement::ConstAccessor<char> acc_bjet("bjet");
const static SG::AuxElement::ConstAccessor<int> acc_charge("MyCharge");
const static SG::AuxElement::ConstAccessor<float> acc_z0sinTheta("z0sinTheta");
const static SG::AuxElement::ConstAccessor<float> acc_d0sig("d0sig");
const static SG::AuxElement::ConstAccessor<int> acc_truthOrigin("truthOrigin");

static pt_greater PtGreater;
static pt_greaterJet PtGreaterJet;


Looper::Looper() :
    m_debug(0),
    //
    m_entry(0),
    m_chain(nullptr),
    //
    m_event(nullptr),
    m_store(nullptr),
    m_evtInfo(nullptr),
    //
    m_susyObj(nullptr),
    m_grlTool(""),
    m_isoTool(""),
    //
    m_isMC(false),
    m_isAFII(false),
    m_mctype(""),
    //
    m_aodElectrons(nullptr),
    m_electronsAux(nullptr),
    m_aodMuons(nullptr),
    m_muonsAux(nullptr),
    m_aodJets(nullptr),
    m_jetsAux(nullptr),
    m_metCont(nullptr),
    m_metAux(nullptr)
{
    m_event = new xAOD::TEvent(xAOD::TEvent::kClassAccess);
    m_store = new xAOD::TStore();
}

void Looper::Init(TTree* tree)
{
    m_event->readFrom(tree);
    m_event->getEntry(0); // this is needed so that we can grab the EventInfo object
    retrieveEventInfo(m_event, m_evtInfo);
    
    ////////////////////////////////////////////////////////////////////////////
    // Get sample metadata
    m_isMC = m_evtInfo->eventType( xAOD::EventInfo::IS_SIMULATION );
    
    if (m_isMC) {
        // Determine MC simulation flavor and campaign
        const xAOD::FileMetaData* fmd = nullptr;
        retrieveFileMetaData(m_event, fmd);
        
        string amitag, simflavor;
        fmd->value(xAOD::FileMetaData::amiTag, amitag);
        fmd->value(xAOD::FileMetaData::simFlavour, simflavor);
        std::transform(simflavor.begin(), simflavor.end(), simflavor.begin(), ::toupper);

        m_isAFII = simflavor.find("ATLFASTII") != std::string::npos;

        if(amitag.find("r9364") != std::string::npos) m_mctype = "mc16a";
        else if(amitag.find("r9781") != std::string::npos) m_mctype = "mc16c";
        else if(amitag.find("r10201") != std::string::npos) m_mctype = "mc16d";
        else if(amitag.find("r10724") != std::string::npos) m_mctype = "mc16e";
        else {
            cout << "    ERROR Failed to determine MC campaign!" << endl;
            exit(1);
        }

        cout << "  Treating sample as " << (m_isAFII ? "AFII" : "FullSim") << endl;
        cout << "  Treating MC sample as MC campaign " << m_mctype << endl;
    } else {
        cout << "  Treating sample as Data\n";
    }

    ////////////////////////////////////////////////////////////////////////////
    // Initialize Good Run List
    initialize_grlTool();

    initialize_isoTools();

    // Initialize SUSYTools Object
    if(!initialize_susytools(m_isMC, m_isAFII)) {
        cout << "    ERROR Failed to initialize SUSYTools. Exitting." << endl;
        exit(1);
    }
}

void Looper::Begin(TTree* /*tree*/){}

Bool_t Looper::Process(Long64_t entry) 
{
    clearObjects();
    m_entry = entry;
    if (m_entry % 1000 == 0) {
        cout << "INFO :: Processing Entry " << m_entry << '\n';
    }
    m_event->getEntry(m_entry);
    retrieveEventInfo(m_event, m_evtInfo);
    m_susyObj->ApplyPRWTool();
    
    int evtNum = m_evtInfo->eventNumber();
    vector<int> evts_to_check = {
        105243138, 
        105163779, 
        105146372, 
        105108486, 
        105205270, 
        105177624, 
        105177631, 
        105244706, 
        105138723, 
        105122873, 
        105113146, 
        105115199, 
        105287232, 
        105153094, 
        105122376, 
        105244234, 
        105300557, 
        105114704, 
        105145938, 
        105244759, 
        105096286, 
        105204324, 
        105138789, 
        105178227, 
        105126516, 
        105193081, 
        105249914, 
        105105531, 
        105161340, 
        105153664, 
        105104007, 
        105115074, 
        105249935, 
        105104528, 
        105163921, 
        105109141, 
        105306264, 
        105146523, 
        105153692, 
        105179814, 
        105005226, 
        105109168, 
        105178289, 
        105115317, 
        105302201, 
        105161915, 
        105209021, 
        105172685, 
        105163984, 
        105179346, 
        105178323, 
        105301722, 
        105160923, 
        105308282, 
        105095390, 
        105109728, 
        105249507, 
        105204453, 
        105114854, 
        105308398, 
        105127162, 
        105114880, 
        105202433, 
        105126151, 
        105301769, 
        105137932, 
        105305871, 
        105246993, 
        105137429, 
        105159958, 
        105108247, 
        105172698, 
        105105718, 
        105174327, 
        105308472, 
        105300794, 
        105204539, 
        105112380, 
        105123137, 
        105122114, 
        105308488, 
        105180491, 
        105163600, 
        105127224, 
        105243986, 
        105149779, 
        105111382, 
        105287513, 
        105285861, 
        105180512, 
        105246052, 
        105146733, 
        105104238, 
        105287536, 
        105138039, 
        105302393, 
        105172608, 
        105178496, 
        105095553, 
        105163139, 
        105122693, 
        105285441, 
        105305998, 
        105154454, 
        105104282, 
        105177499, 
        105205151, 
        105149860, 
        105209767, 
        105201064, 
        105206708, 
        105244087, 
        105127362, 
        105149379, 
        105287634, 
        105161690, 
        105163231, 
        105205217, 
        105240546, 
        105177064, 
        105153534, 
        105126738, 
        105206766, 
        105308661, 
        105150456, 
        105206266, 
        105246378, 
        105209854,
        -1 // Dummy
    };
    auto it = std::find(evts_to_check.begin(), evts_to_check.end(), evtNum); 
    bool dump_event = (it == evts_to_check.end()) ? false : true;
    if (evts_to_check.size() > 1 && !dump_event) return true;

    ////////////////////////////////////////////////////////////////////////////
    // Get event objects and fill into sorted containers
    RETURN_CHECK( GetName(), m_susyObj->GetElectrons(m_aodElectrons, m_electronsAux, true) );
    RETURN_CHECK( GetName(), m_susyObj->GetMuons(m_aodMuons, m_muonsAux, true) );
    RETURN_CHECK( GetName(), m_susyObj->GetJets(m_aodJets, m_jetsAux) );
    
    ////////////////////////////////////////////////////////////////////////////
    // Overlap removal
    RETURN_CHECK( GetName(), m_susyObj->OverlapRemoval(m_aodElectrons, m_aodMuons, m_aodJets));

    ////////////////////////////////////////////////////////////////////////////
    // Get MissingET
    m_metCont = new xAOD::MissingETContainer();
    m_metAux = new xAOD::MissingETAuxContainer();
    m_metCont->setStore(m_metAux);
    m_metCont->reserve(10);
    RETURN_CHECK( GetName(), m_susyObj->GetMET(*m_metCont, m_aodJets, m_aodElectrons, m_aodMuons) );
    xAOD::MissingETContainer::const_iterator met_it = m_metCont->find("Final");
    m_metTLV.SetPtEtaPhiE((*met_it)->met(), 0, (*met_it)->phi(), (*met_it)->met());

    ////////////////////////////////////////////////////////////////////////////
    // Build pT ordered object collections
    for (const auto &e : *m_aodElectrons) { 
        dec_charge(*e) = e->charge(); 
        m_preElectrons.push_back(e);
        m_preLeptons.push_back(e);
        if (!acc_passOR(*e) || fabs(e->eta()) >= 2.47) continue;
        if(acc_baseline(*e)) {
            m_baseElectrons.push_back(e);
            m_baseLeptons.push_back(e);
        }
        if(acc_signal(*e)) {
            m_signalElectrons.push_back(e);
            m_signalLeptons.push_back(e);
        }
    }
    std::sort(m_preElectrons.begin(), m_preElectrons.end(), PtGreater);
    std::sort(m_baseElectrons.begin(), m_baseElectrons.end(), PtGreater);
    std::sort(m_signalElectrons.begin(), m_signalElectrons.end(), PtGreater);
    
    for (const auto &m : *m_aodMuons) { 
        dec_charge(*m) = m->charge(); 
        m_preMuons.push_back(m);
        m_preLeptons.push_back(m);
        if (!acc_passOR(*m) || fabs(m->eta()) >= 2.7) continue;
        if(acc_baseline(*m)) {
            m_baseMuons.push_back(m);
            m_baseLeptons.push_back(m);
        }
        if(acc_signal(*m)) { 
            m_signalMuons.push_back(m);
            m_signalLeptons.push_back(m);
        }
    }
    std::sort(m_preMuons.begin(), m_preMuons.end(), PtGreater);
    std::sort(m_baseMuons.begin(), m_baseMuons.end(), PtGreater);
    std::sort(m_signalMuons.begin(), m_signalMuons.end(), PtGreater);
    
    std::sort(m_preLeptons.begin(), m_preLeptons.end(), PtGreater);
    std::sort(m_baseLeptons.begin(), m_baseLeptons.end(), PtGreater);
    std::sort(m_signalLeptons.begin(), m_signalLeptons.end(), PtGreater);
    
    for(const auto &j : *m_aodJets) { 
        m_susyObj->IsBJet(*j); 
        m_preJets.push_back(j);
        if(acc_baseline(*j) && acc_passOR(*j)) m_baseJets.push_back(j);
        if(acc_signal(*j) && acc_passOR(*j)) m_signalJets.push_back(j);
    }
    std::sort(m_preJets.begin(), m_preJets.end(), PtGreaterJet);
    std::sort(m_baseJets.begin(), m_baseJets.end(), PtGreaterJet);
    std::sort(m_signalJets.begin(), m_signalJets.end(), PtGreaterJet);
    ////////////////////////////////////////////////////////////////////////////
    // Dump Specific Event Info
    ////////////////////////////////////////////////////////////////////////////
    bool print_cutflow = true;
    if (dump_event) {
        cout << "============================================================\n";
        cout << "VERBOSE :: Dumping event info for Event " << evtNum << '\n';
        
        int count = 0;
        for (xAOD::IParticle* lep : m_preLeptons ) {
            if (!acc_baseline(*lep)) continue;
            cout << "VERBOSE :: xAOD Lepton " << ++count << ") "
                 << lepton_info_str(lep);
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Cutflow 
    ////////////////////////////////////////////////////////////////////////////
    
    m_ctr.readin += 1;
    
    // 1) Detector error veto
    // GRL
    int runNumber = m_evtInfo->runNumber();
    int lumiBlock = m_evtInfo->lumiBlock();
    bool pass_grl = (m_isMC ? true : m_grlTool->passRunLB(runNumber, lumiBlock));
    //if(!pass_grl) {}

    // Event Cleaning
    bool passCleaning = !(
        (m_evtInfo->errorState(xAOD::EventInfo::LAr)  == xAOD::EventInfo::Error ) ||
        (m_evtInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) ||
        (m_evtInfo->errorState(xAOD::EventInfo::SCT)  == xAOD::EventInfo::Error ) ||
        (m_evtInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) )
    );
    if (!pass_grl || !passCleaning) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 01 " << m_ctr.detErr.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - "
                 << "LAr = " << (m_evtInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) << ", "
                 << "Tile = " << (m_evtInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error) << ", "
                 << "SCT = " << (m_evtInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error) << ", "
                 << "TTC = " << (m_evtInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18)) << '\n' ;
        }
        return false;
    }
    m_ctr.detErr += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 2) Primary vertex
    bool pass_good_vtx = (m_susyObj->GetPrimVtx() == nullptr) ? false : true;
    if(!pass_good_vtx) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 02 " << m_ctr.goodVtx.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.goodVtx += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 3) Fired trigger
    bool trig_fired = false;
    for (string trig_name : CONFIG::em_trigs) {
        if (m_susyObj->IsTrigPassed(trig_name)) trig_fired = true;
    }
    for (string trig_name : met_trigs()) {
        if (m_susyObj->IsMETTrigPassed(trig_name)) trig_fired = true;
    }
    if (!trig_fired) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 03 " << m_ctr.trig.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.trig += 1;
    ////////////////////////////////////////////////////////////////////////////
    // 4) >=2 baseline leptons before OR
    int n_baseLepBeforeOR = 0;
    for (xAOD::IParticle* lep : m_preLeptons ) {
        if (acc_baseline(*lep)) n_baseLepBeforeOR++;
    }
    if (n_baseLepBeforeOR < 2) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 04 " << m_ctr.nBaseLepNoOR.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - n_baseLepBeforeOR = " << n_baseLepBeforeOR << "\n";
        }
        return false;
    }

    m_ctr.nBaseLepNoOR += 1;
    ////////////////////////////////////////////////////////////////////////////
    // 5) Event cleaning cuts

    // Bad or Cosmic Muon
    bool pass_bad_mu = true;
    bool pass_cosmic = true;
    for(const auto & mu : m_preMuons) {
        if (acc_baseline(*mu) && acc_bad(*mu)) pass_bad_mu = false;
        //if (acc_passOR(*mu) && acc_cosmic(*mu)) pass_cosmic = false;
    }
    // jet cleaning
    bool pass_jet_cleaning = true;
    for(const auto & jet : m_baseJets) {
        if (acc_bad(*jet)) pass_jet_cleaning = false;
    }
    if(!pass_bad_mu || !pass_cosmic || !pass_jet_cleaning) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 05 " << m_ctr.cleanVeto.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - bad mu = " << !pass_bad_mu  
                    << "; cosmic mu = " <<  !pass_cosmic 
                    << "; bad jet = " << !pass_jet_cleaning 
                    << "\n";
        }
        return false;
    }

    m_ctr.cleanVeto += 1;
    ////////////////////////////////////////////////////////////////////////////
    // 6) 2 baseline leptons after OR (with added eta cuts)
    if (m_baseLeptons.size() != 2) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 06 " << m_ctr.nBaseLep.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }

    m_ctr.nBaseLep += 1;
    ////////////////////////////////////////////////////////////////////////////
    // 7) Opposite sign
    int lep1Q = acc_charge(*m_baseLeptons.at(0));
    int lep2Q = acc_charge(*m_baseLeptons.at(1));
    bool OS = ((lep1Q * lep2Q) < 0);
    if (!OS) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 07 " << m_ctr.OS.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }

    m_ctr.OS += 1;
    ////////////////////////////////////////////////////////////////////////////
    // 8) Isolation requirement
    bool passIso = true;
    for(const auto & e : m_baseElectrons) {
        if (!m_isoTool->accept(*e)) passIso = false;
    }
    for(const auto & m : m_baseMuons) {
        if (!m_isoTool->accept(*m)) passIso = false;
    }
    if (!passIso) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 08 " << m_ctr.iso.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
     

    m_ctr.iso += 1;
    ////////////////////////////////////////////////////////////////////////////
    // 9) 2 signal leptons
    if (m_signalLeptons.size() != 2) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 09 " << m_ctr.nSigLep.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.nSigLep += 1;


    ////////////////////////////////////////////////////////////////////////////
    // 10) Truth Origin
    bool passTruth = true;
    for (const auto & lep : m_signalLeptons) {
        switch (acc_truthOrigin(*lep)) {
            case 10 : // Top 
            case 12 : // W
            case 13 : // Z
            case 14 : // Higgs
            case 43 : // VV
                      break;
            default : passTruth = false;
        }
    }
    if (!passTruth) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 10 " << m_ctr.truthO.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.truthO += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 11) Eta
    bool passEta = true;
    for (const auto & e : m_signalElectrons) {
       if (fabs(e->eta()) > 2.47) passEta = false;
    }
    for (const auto & m : m_signalMuons) {
       if (fabs(m->eta()) > 2.40) passEta = false;
    }
    if (!passEta) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 11 " << m_ctr.eta.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.eta += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 12) Trigger matching
    bool passTrigMatch = false;

    vector<const xAOD::IParticle*> trig_candidates;
    trig_candidates.push_back(m_signalLeptons.at(0));
    trig_candidates.push_back(m_signalLeptons.at(1));
    bool trigFired = m_susyObj->IsTrigPassed("HLT_e17_lhloose_nod0_mu14");
    bool isDF = m_signalElectrons.size() == 1 && m_signalMuons.size() == 1;
    if (trigFired && isDF) {
        passTrigMatch = m_susyObj->IsTrigMatched(trig_candidates, "HLT_e17_lhloose_nod0_mu14");
    }
    if (!passTrigMatch) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 12 " << m_ctr.trigMatch.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - "
                 << "Trig fired? " << trigFired << ", "
                 << "isDF? " << isDF << ", "
                 << "Trig matched? " << passTrigMatch << "\n";
        }
        return false;
    }
    m_ctr.trigMatch += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 13) Mll
    bool passMll = true;
    
    TLorentzVector p1 = to_TLorentzVector(m_signalLeptons.at(0));
    TLorentzVector p2 = to_TLorentzVector(m_signalLeptons.at(1));
    TLorentzVector dilepP4 = p1 + p2;
    passMll = dilepP4.M() > 20000; //MeV

    if (!passMll) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 13 " << m_ctr.mll.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.mll += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 14) l1 pT
    bool passl1Pt = m_signalLeptons.at(0)->pt() > 25000; //MeV
    if (!passl1Pt) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 14 " << m_ctr.l1Pt.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.l1Pt += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 15) l2 pT
    bool passl2Pt = m_signalLeptons.at(1)->pt() > 20000; //MeV
    if (!passl2Pt) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 15 " << m_ctr.l2Pt.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.l2Pt += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 16) MET
    bool passMET = (*m_metCont->find("Final"))->met() > 250000; //MeV
    if (!passMET) {
        if (print_cutflow) {
            cout << "CUTFLOW" << " : "
                 << "Cut 16 " << m_ctr.met.name << " : "
                 << "Event " << evtNum << " : "
                 << "Info - None\n";
        }
        return false;
    }
    m_ctr.met += 1;

    ////////////////////////////////////////////////////////////////////////////
    // 17) Passed Cutflow
    if (print_cutflow) {
        cout << "CUTFLOW" << " : "
             << "Cut 17 Passed" << " : "
             << "Event " << evtNum << " : "
             << "Info - None\n";
    }
    return 0;
}

void Looper::Terminate(){
    m_ctr.Print();
}

void Looper::clearObjects() {
    m_store->clear();
    
    m_electronsAux = nullptr;
    m_aodElectrons = nullptr;
    m_preElectrons.clear();
    m_baseElectrons.clear();
    m_signalElectrons.clear();
    m_muonsAux = nullptr;
    m_aodMuons = nullptr;
    m_preMuons.clear();
    m_baseMuons.clear();
    m_signalMuons.clear();
    m_preLeptons.clear();
    m_baseLeptons.clear();
    m_signalLeptons.clear();
    m_jetsAux = nullptr;
    m_aodJets = nullptr;
    m_preJets.clear();
    m_baseJets.clear();
    m_signalJets.clear();
    if(m_metCont) { delete m_metCont; m_metCont = nullptr; }
    if(m_metAux) { delete m_metAux; m_metAux = nullptr; }
    m_metTLV = TLorentzVector();

}

bool Looper::retrieveEventInfo(xAOD::TEvent* evt, const xAOD::EventInfo*& ei) {
    RETURN_CHECK( GetName(), evt->retrieve(ei, "EventInfo"));
    return true;
}
bool Looper::retrieveFileMetaData(xAOD::TEvent* evt, const xAOD::FileMetaData*& fmd) {
    RETURN_CHECK( GetName(), evt->retrieveMetaInput( fmd, "FileMetaData" ) );
    return true;
}

bool Looper::initialize_grlTool() {
    m_grlTool.setTypeAndName("GoodRunsListSelectionTool/grl");
    m_grlTool.isUserConfigured();
    vector<string> grl_files = resolvePaths(CONFIG::grl_files);
    RETURN_CHECK( GetName(), m_grlTool.setProperty("GoodRunsListVec", grl_files) );
    RETURN_CHECK( GetName(), m_grlTool.setProperty("PassThrough", false) );
    RETURN_CHECK( GetName(), m_grlTool.retrieve() );
    return true;
}

bool Looper::initialize_isoTools() {
    // Gradient WP for leptons, FixedCutTightCaloOnly WP for photons
    if(!m_isoTool.isUserConfigured()) {
        m_isoTool.setTypeAndName("CP::IsolationSelectionTool/IsoTool");
        RETURN_CHECK( GetName(), m_isoTool.setProperty("ElectronWP", "Gradient") );
        RETURN_CHECK( GetName(), m_isoTool.setProperty("MuonWP",     "FCLoose") );
        RETURN_CHECK( GetName(), m_isoTool.retrieve() );
    } // configured
    return true;
}

bool Looper::initialize_susytools(bool isMC, bool isAFII)
{
    ////////////////////////////////////////////////////////////////////////////
    // Instantiate new SUSYTools Object
    string name = "SUSYToolsTrigTest";
    m_susyObj = new ST::SUSYObjDef_xAOD(name);
    
    if (m_debug > 0) {
        cout << "DEBUG :: Setting SUSYTools to run in debug mode\n";
        m_susyObj->msg().setLevel(MSG::DEBUG);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Configure for input sample
    ST::ISUSYObjDef_xAODTool::DataSource datasource;
    if (!isMC) { 
        datasource = ST::ISUSYObjDef_xAODTool::Data;
    } else if (isAFII) {
        datasource = ST::ISUSYObjDef_xAODTool::AtlfastII;
    } else {
        datasource = ST::ISUSYObjDef_xAODTool::FullSim;
    }
    RETURN_CHECK( GetName(), m_susyObj->setProperty("DataSource", datasource) );
    RETURN_CHECK( GetName(), m_susyObj->setProperty("mcCampaign", m_mctype));


    ////////////////////////////////////////////////////////////////////////////
    // Load configuration files
    string configFile = "STCutflowLooper/Stop2L.conf";
    RETURN_CHECK( GetName(), m_susyObj->setProperty("ConfigFile", configFile));
    
    vector<string> lumicalcs = resolvePaths(CONFIG::ilumicalc_files);
    RETURN_CHECK( GetName(), m_susyObj->setProperty("PRWLumiCalcFiles", lumicalcs) );

    vector<string> prw_configs = prw_files(m_isAFII);
    RETURN_CHECK( GetName(), m_susyObj->setProperty("PRWConfigFiles", prw_configs) );

    cout << "\n===================================================" << endl;
    cout << "  Loading ilumicalc files into SUSYObj:" << endl;
    for(auto calc : lumicalcs) { cout << "    > " << calc << endl; }
    cout << "  Loading prw config files into SUSYObj:" << endl;
    for(auto conf : prw_configs) { cout << "   > " << conf << endl; }
    cout << "===================================================\n" << endl;
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Final check
    if(m_susyObj->initialize() != StatusCode::SUCCESS) {
        cout << "   ERROR Cannot initialize SUSYTools, exitting." << endl;
        return false;
    }
    return true;
}

vector<string> Looper::resolvePaths(vector<string> paths) {
    vector<string> resolvedPaths;
    bool allPathsResolved = true;

    for (string relPath : paths) {
        string absPath = PathResolverFindCalibFile(relPath);
        if(absPath=="") {
            cout << "    ERROR PathResolver unable to resolve path "
                 << " (=" << relPath << ")" << endl;
            allPathsResolved = false;
        } else {
            resolvedPaths.push_back(absPath);
        }
    }
    
    if(!allPathsResolved) exit(1);
    return resolvedPaths;

}

vector<string> Looper::prw_files(bool isAFII) {
    vector<string> prw_out;
    
    // Build PRW directory for sample
    // Example: path/to/dev/DSID456xxx/
    string dsid = std::to_string(m_evtInfo->mcChannelNumber());
    string dev_dir = CONFIG::prw_dev_dir;
    string prw_config_loc = dev_dir + "/DSID" + dsid.substr(0,3) + "xxx";

    // Resolve PRW directory path
    string prw_config = PathResolverFindCalibDirectory(prw_config_loc);
    if(prw_config == "") {
        cout << "    WARNING Could not locate "
             << "group area (=" << prw_config_loc << ")"
             << ", failed to find PRW config files for sample!\n";
        exit(1);
    }
    
    // Check for PRW files in the three main MC campaigns
    string sim = isAFII ? "AFII" : "FS";
    cout << "================================================" << endl;
    cout << "PRW CONFIG directory : " << prw_config << endl;
    for (string c : {"mc16a","mc16d","mc16e"}) {
        // Example :: pileup_mc16a_dsid123456_FS.root
        string fname = "pileup_" + c + "_dsid" + dsid + "_" + sim + ".root";
        string f = prw_config + "/" + fname;
        TFile test(f.data(), "read");
        if(test.IsZombie()) {
            cout << "    ERROR Unable to open the " << c 
                 << " PRW config file (=" << f << ")" << endl;
            exit(1);
        } else {
            cout <<  "  >> File opened OK (" << f << ")" << endl;
        }
        prw_out.push_back(f);
    }
    cout << "================================================" << endl;

    return prw_out;
}

vector<string> Looper::single_lepton_trigs() {
    vector<string> trigs;
    for (string trig : CONFIG::e_trigs){ trigs.push_back(trig); }
    for (string trig : CONFIG::m_trigs){ trigs.push_back(trig); }
    return trigs;
}
vector<string> Looper::dilepton_trigs() {
    vector<string> trigs;
    for (string trig : CONFIG::ee_trigs){ trigs.push_back(trig); }
    for (string trig : CONFIG::ee_trigs){ trigs.push_back(trig); }
    for (string trig : CONFIG::em_trigs){ trigs.push_back(trig); }
    for (string trig : CONFIG::me_trigs){ trigs.push_back(trig); }
    return trigs;
}
vector<string> Looper::lepton_trigs() {
    vector<string> trigs;
    for (string trig : single_lepton_trigs()){ trigs.push_back(trig); }
    for (string trig : dilepton_trigs()){ trigs.push_back(trig); }
    return trigs;
}
vector<string> Looper::met_trigs() { return CONFIG::met_trigs; }

string Looper::lepton_info_str(const xAOD::IParticle* p) {
    stringstream ss;
    auto e = dynamic_cast<const xAOD::Electron*>(p);
    auto m = dynamic_cast<const xAOD::Muon*>(p);
    if (e != nullptr) { ss << "El - "; }
    else if (m != nullptr) { ss << "Mu - "; }
    else {ss << "?? - ";}
    ss << "(pt,eta,phi,Q) = (" 
       << p->pt() << "MeV, " 
       << p->eta() << ", " 
       << p->phi() << ", " 
       << acc_charge(*p) << "); ";
    ss << "(baseline, passOR, signal) = (" 
        << (acc_baseline(*p) ? "True" : "False") << ", "
        << (acc_passOR(*p) ? "True" : "False") << ", "
        << (acc_signal(*p) ? "True" : "False") << "); ";
    if (e != nullptr) { ss << "isoGradient = " << m_isoTool->accept(*e) << "; "; }
    else if (m != nullptr) { ss << "isoFCLoose = " << m_isoTool->accept(*m) << "; "; }
    ss << "\n";
    return ss.str();
}

TLorentzVector Looper::to_TLorentzVector(const xAOD::IParticle* p) {
    TLorentzVector pTLV;
    pTLV.SetPtEtaPhiE(p->pt(), p->eta(), p->phi(), p->e());
    return pTLV;
}
