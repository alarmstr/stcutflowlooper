#pragma once

#include <vector>
using std::vector;
#include <string>
using std::string;

namespace CONFIG {
    // Good Run Lists for 2015-2018
    vector<string> grl_files = {
        "GoodRunsLists/data15_13TeV/20170619/data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml",
        "GoodRunsLists/data16_13TeV/20180129/data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml",
        "GoodRunsLists/data17_13TeV/20180619/data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml",
        "GoodRunsLists/data18_13TeV/20180906/data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"
    };

    // Instantaneous luminosity calculation files for 2015-2018
    vector<string> ilumicalc_files = {
        "GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        "GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root",
        "GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root",
        "GoodRunsLists/data18_13TeV/20181111/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root"
    };

    string prw_dev_dir = "dev/PileupReweighting/share";
    string prw_dev_dir_old = "dev/PileupReweighting/mc16_13TeV/";

    vector<string> e_trigs = {
        // 2015
        "HLT_e24_lhmedium_L1EM20VH",
        "HLT_e60_lhmedium",
        "HLT_e120_lhloose",
        // 2016-2018
        "HLT_e26_lhtight_nod0_ivarloose",
        "HLT_e60_lhmedium_nod0",
        "HLT_e140_lhloose_nod0",
        // 2018
        "HLT_e26_lhtight_iloose"

    };
    vector<string> ee_trigs = {
        // 2015
        "HLT_2e12_lhloose_L12EM10VH",
        // 2016
        "HLT_2e17_lhvloose_nod0",
        // 2017-2018
        "HLT_2e24_lhvloose_nod0",
        // 2018
        // L1_2EM15VHI was accidentally prescaled in periods B5-B8 of 2017
        // (runs 326834-328393) with an effective reduction of 0.6 fb-1
        "HLT_2e17_lhvloose_nod0_L12EM15VHI"
    };
    vector<string> m_trigs = {
        // 2015
        "HLT_mu20_iloose_L1MU15",
        "HLT_mu40",
        // 2016-2018
        "HLT_mu26_ivarmedium",
        "HLT_mu50"

    };
    vector<string> mm_trigs = {
        // 2015
        "HLT_2mu10",
        "HLT_mu18_mu8noL1",
        // 2016-2018
        "HLT_2mu14",
        "HLT_mu22_mu8noL1"

    };
    vector<string> em_trigs = {
        // 2015
        //"HLT_e17_lhloose_mu14",
        // 2016
        //"HLT_e26_lhmedium_nod0_L1EM22VHI_mu8noL1",
        // 2016-2018
        "HLT_e17_lhloose_nod0_mu14",
        // 2017-2018
        //"HLT_e26_lhmedium_nod0_mu8noL1"
    };
    vector<string> me_trigs = {
        // 2015
        "HLT_e7_lhmedium_mu24",
        // 2016-2018
        "HLT_e7_lhmedium_nod0_mu24"
    };
    vector<string> met_trigs = {
        "HLT_xe110_pufit_xe65_L1XE50"
    };
}
