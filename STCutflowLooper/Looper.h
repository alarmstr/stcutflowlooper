#pragma once

//C++
#include <vector>
using std::vector;
#include <string>
using std::string;
#include <iostream>
using std::cout;
#include <iomanip>
using std::setw;

//ROOT
#include "TSelector.h"
#include "TChain.h"
#include "TTree.h"
#include "TFile.h"
#include "TLorentzVector.h"

//xAOD
#include "xAODEventInfo/EventInfo.h"
#include "xAODMetaData/FileMetaData.h"

//CONTAINERS
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

//ASG
#include "AsgTools/ToolHandle.h"
#include "AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h"
#include "IsolationSelection/IIsolationSelectionTool.h"

// SUSYTools
#include "SUSYTools/SUSYObjDef_xAOD.h"

////////////////////////////////////////////////////////////////////////////////
// Structs for tracking cutflow
struct Count {
    int raw;
    double weighted;
    string name;

    Count(const string& newName) : raw(0), weighted(0.0), name(newName) {}
    
    void operator+=(double w) {
        raw++;
        weighted += w;
    };

    void Print(const string& tabs="", int width=0) {
        cout << tabs << setw(width) << name << ": " << weighted << "  [" << raw << "]\n";    
    };
};

struct CutflowCounter {
    Count readin{"Read in"};
    Count detErr{"No detector errors"};
    Count goodVtx{"Has a good vertex"};
    Count trig{"Passed trigger"};
    Count nBaseLepNoOR{"2+ baseline leptons before OR"};
    Count cleanVeto{"Passed cleaning cuts"};
    Count nBaseLep{"2 base leptons after OR"};
    Count OS{"Opposite sign"};
    Count iso{"Isolated leptons"};
    Count nSigLep{"2 signal leptons"};
    Count truthO{"t,W,Z,H,VV truth origin"};
    Count eta{"eta < 2.47 (2.4) ele (mu)"};
    Count trigMatch{"HLT_e17_lhloose_nod0_mu14"};
    Count mll{"m_ll > 20 GeV"};
    Count l1Pt{"leading lepton pt>25 GeV"};
    Count l2Pt{"subleading lepton pt>20 GeV"};
    Count met{"MET>250 GeV"};



    std::vector<Count*> countList = {
        &readin, &detErr, &goodVtx, &trig, &nBaseLepNoOR, 
        &cleanVeto, &nBaseLep, &OS, &iso, &nSigLep,
        &truthO, &eta, &trigMatch, &mll, &l1Pt, &l2Pt, &met
    };

    int longestNameLen() {
        uint maxLen = 0; 
        for (Count* c : countList) {
            if (c->name.size() > maxLen) maxLen = c->name.size();
        }
        return maxLen;
    }

    void Print() {
        uint spacing = longestNameLen() + 1;
        cout << "Cutflow results\n";
        for (Count* c : countList) c->Print("\t", spacing);
        cout << "\n";
        
        cout << "For copying (weighted)\n";
        cout.precision(0);
        cout << std::fixed;
        for (Count* c : countList) cout << c->weighted << '\n';
        cout << "\n";
        
        cout << "For copying (raw)\n";
        for (Count* c : countList) cout << c->raw << '\n';
    }
};

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// Main Looper
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
class Looper : public TSelector
{
public:
    Looper();
    virtual ~Looper(){};

    void set_chain(TChain* chain) {m_chain = chain;}
    void set_debug_level(int debug) {m_debug = debug;}

    //////////////////////////////////////////////////////
    // TSelector overrides
    //////////////////////////////////////////////////////
    virtual void Init(TTree* tree); // Called every time a new TTree is attached
    //virtual void SlaveBegin(TTree* tree);
    virtual void Begin(TTree* tree); // Called before looping on entries
    virtual Bool_t Notify() { return kTRUE; } // Called at the first entry of a new file
    virtual void Terminate(); // called after looping is finished
    virtual Int_t Version() const { return 2; } // adhere to stupid ROOT design
    virtual Bool_t Process(Long64_t entry); // main event loop function

    //////////////////////////////////////////////////////
    // Other Stuff
    //////////////////////////////////////////////////////
            


private:
    // Configuration settings
    int m_debug;
    
    // ROOT objects for looping over TTree
    Long64_t m_entry;
    TChain* m_chain;

    // xAOD objects for looping over xAOD TTree
    xAOD::TEvent* m_event;
    xAOD::TStore* m_store; // Needed to suppress warnings
    const xAOD::EventInfo* m_evtInfo;

    // Tools for processing xAOD
    ST::SUSYObjDef_xAOD* m_susyObj;
    asg::AnaToolHandle<IGoodRunsListSelectionTool> m_grlTool;
    asg::AnaToolHandle<CP::IIsolationSelectionTool> m_isoTool;

    // Flags
    bool m_isMC;
    bool m_isAFII;
    string m_mctype;

    // Containers for storing xAOD contents
    // aod = objects read from xAOD
    // pre = aod but pT sorted
    // base = subset of pre passing baseline selection and OR
    // signal = subset of base passing signal selection
    xAOD::ElectronContainer* m_aodElectrons;
    xAOD::ShallowAuxContainer* m_electronsAux;
    vector<xAOD::Electron*> m_preElectrons;
    vector<xAOD::Electron*> m_baseElectrons;
    vector<xAOD::Electron*> m_signalElectrons;
    
    xAOD::MuonContainer* m_aodMuons;
    xAOD::ShallowAuxContainer* m_muonsAux;
    vector<xAOD::Muon*> m_preMuons;
    vector<xAOD::Muon*> m_baseMuons;
    vector<xAOD::Muon*> m_signalMuons;
    
    vector<xAOD::IParticle*> m_preLeptons;
    vector<xAOD::IParticle*> m_baseLeptons;
    vector<xAOD::IParticle*> m_signalLeptons;
    
    xAOD::JetContainer* m_aodJets;
    xAOD::ShallowAuxContainer* m_jetsAux;
    vector<xAOD::Jet*> m_preJets;
    vector<xAOD::Jet*> m_baseJets;
    vector<xAOD::Jet*> m_signalJets;

    xAOD::MissingETContainer* m_metCont;
    xAOD::MissingETAuxContainer* m_metAux;
    TLorentzVector m_metTLV;
    
    ////////////////////////////////////////////////////////////////////////////
    // Functions
    void clearObjects();
    bool retrieveEventInfo(xAOD::TEvent* evt, const xAOD::EventInfo*& ei);
    bool retrieveFileMetaData(xAOD::TEvent* evt, const xAOD::FileMetaData*& fmd);
    bool initialize_grlTool();
    bool initialize_isoTools();
    bool initialize_susytools(bool isMC, bool isAFII);

    bool verbose() {return m_debug >= 2;}
    bool debug() {return m_debug >= 1;}

    vector<string> resolvePaths(vector<string> paths);
    vector<string> prw_files(bool isAFII);
    vector<string> single_lepton_trigs();
    vector<string> dilepton_trigs();
    vector<string> lepton_trigs();
    vector<string> met_trigs();

    TLorentzVector to_TLorentzVector(const xAOD::IParticle* p);
    
    string lepton_info_str(const xAOD::IParticle* p);
    ////////////////////////////////////////////////////////////////////////////
    // Cutflow
    CutflowCounter m_ctr;

}; // class Looper

////////////////////////////////////////////////////////////////////////////////
// Structs for sorting particle containers
struct pt_greater {
    bool operator() (const xAOD::IParticle* a, const xAOD::IParticle* b) {
        return a->pt() > b->pt(); 
    }
};

struct pt_greaterJet {
    bool operator() (const xAOD::Jet* a, const xAOD::Jet* b) { 
        return a->pt() > b->pt(); 
    }
};

