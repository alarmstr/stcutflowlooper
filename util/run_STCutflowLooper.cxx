////////////////////////////////////////////////////////////////////////////////
/// Copyright (c) <2019> by Alex Armstrong
///
/// @file run_STCutflowLooper.cxx
/// @author Alex Armstrong <alarmstr@cern.ch>
/// @date <02 Apr 2019>
/// @brief Simple looper using bare bones SUSYTools
///
////////////////////////////////////////////////////////////////////////////////

#include "STCutflowLooper/Looper.h"
#include "TChain.h"

#include <iostream>
#include <string>

using std::string;
using std::cout;
using std::cerr;

////////////////////////////////////////////////////////////////////////////////
// Declarations
////////////////////////////////////////////////////////////////////////////////
/// @brief Command line argument parser
struct Args {
    ////////////////////////////////////////////////////////////////////////////
    // Initialize arguments
    ////////////////////////////////////////////////////////////////////////////
    string PROG_NAME;  // always required

    // Required arguments
    string ifile_name;  ///< Input file name

    // Optional arguments with defaults
    int nevents =  -1; ///< Number of events to process
    int debug = 0; ///< Debug level

    ////////////////////////////////////////////////////////////////////////////
    // Print information
    ////////////////////////////////////////////////////////////////////////////
    void print_usage() const {
        printf("===========================================================\n");
        printf(" %s\n", PROG_NAME.c_str());
        printf(" Simple looper using bare bones SUSYTools\n");
        printf("===========================================================\n");
        printf("Required Parameters:\n");
        printf("\t-f, --input-file      Input file name\n");
        printf("\nOptional Parameters:\n");
        printf("\t-n, --nevents     Number of events to process\n");
        printf("\t-d, --debug       Debug level");
        printf("\t-h, --help        Print this help message\n");
        printf("===========================================================\n");
    }

    void print() const {
        printf("===========================================================\n");
        printf(" %s Configuration\n", PROG_NAME.c_str());
        printf("===========================================================\n");
        printf("\tInput file name:   %s\n", ifile_name.c_str());
        printf("\tEvents to process: %i\n", nevents);
        printf("\tDebug level:       %i\n", debug);
        printf("===========================================================\n");
    }

    ////////////////////////////////////////////////////////////////////////////
    // Parser
    ////////////////////////////////////////////////////////////////////////////
    bool parse(int argc, char* argv[]) {
        PROG_NAME = argv[0];

        // Parse arguments
        for (int i = 0; i< argc; ++i) {
            // Grab arguments
            string arg = argv[i];
            string arg_value = argc > i+1 ? argv[i+1] : "";
            // Skip if arg set to arg value and not arg name
            if (arg.at(0) != '-') continue;

            // Check for arguments
            if (arg == "-f" || arg == "--input-file") {
                ifile_name = arg_value;
            } else if (arg == "-n" || arg == "--nevents") {
                nevents = std::stoi(arg_value);
            } else if (arg == "-d" || arg == "--debug") {
                debug = std::stoi(arg_value);
            } else if (arg == "-h" || arg == "--help") {
                print_usage();
                return false;
            } else {
                cerr << "ERROR :: Unrecognized input argument: "
                     << arg << " -> " << arg_value << '\n';
                print_usage();
            }
        }

        // Check arguments
        if (ifile_name.size() == 0) {
            cerr << "ERROR :: No input file given\n";
            return false;
        }
        return true;
    }
} args;

////////////////////////////////////////////////////////////////////////////////
/// @brief Main function
///
/// Run with help option (-h, --help) to see available parameters
////////////////////////////////////////////////////////////////////////////////
int main(int argc, char* argv[]) {
    cout << "\n====== RUNNING " << argv[0] << " ====== \n";
    if (args.parse(argc, argv)) {
        // Finished parsing arguments
        args.print();
    } else {
        // Failed to parse arguments or help requested
        return 1;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Main implementation
    ////////////////////////////////////////////////////////////////////////////
    TChain* chain = new TChain("CollectionTree");
    chain->Add(args.ifile_name.c_str());
    Long64_t nInChain = chain->GetEntries();
    Long64_t nToProcess = args.nevents;
    if(args.nevents < 0 || args.nevents > nInChain) nToProcess = nInChain;

    Looper* looper = new Looper();
    looper->set_chain(chain);
    looper->set_debug_level(args.debug);
    chain->Process(looper, args.ifile_name.c_str(), nToProcess);

    delete looper;
    delete chain;
    cout << "\n====== SUCCESSFULLY RAN " << argv[0] << " ====== \n";
    return 0;
}

