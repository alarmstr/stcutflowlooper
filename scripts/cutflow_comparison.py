#!/usr/bin/env python
"""
== cutflow_comparison ==
Compare the events processed in two cutflows, providing summary results


EXAMPLES
    python cutflow_comparison AA_cutflow.json matt_events.json

Author: Alex Armstrong <alarmstr@cern.ch>
Copyright: (C) Jan 11, 2018; University of California, Irvine
"""

import sys, os, traceback, argparse
import time
import json
from collections import defaultdict

#------------------------------------------------------------------------------#
# User-configured options

# Cuts to compare between cutflows
# FORMAT SENSATIVE
cuts_to_compare = [
    #('Cut 00 Read in'                         ,'TotalEvents'),
    #('Cut 01 No detector errors'              ,'NoErrFlags'),
    #('Cut 02 Has a good vertex'               ,'PrimaryVx'),
    #('Cut 03 Passed trigger'                  ,'Trigger'),
    #('Cut 04 2+ baseline leptons before OR'   ,'AtLeast2OSLeptons'),
    #('Cut 05 Passed cleaning cuts'            ,'Cut 05 cleaning Veto'),
    ('Cut 06 2 base leptons after OR'      ,'Cut 06 Exactly 2 leptons after OR'),
    ('Cut 07 Opposite sign'                ,'Cut 07 2 OS leptons'),
    ('Cut 08 Isolated leptons'             ,'Cut 08 Isolation: Gradient (FCLoose) for electrons (muons)'),
    ('Cut 09 2 signal leptons'             ,'Cut 09 2 Signal leptons'),
    ('Cut 10 t,W,Z,H,VV truth origin'      ,'Cut 10 Truth Origin'),
    ('Cut 11 eta < 2.47 (2.4) ele (mu)'    ,'Cut 11 eta<2.47 (2.4) for electrons (muons)'),
    ('Cut 12 HLT_e17_lhloose_nod0_mu14'    ,'Cut 12 HLT_e17_lhloose_nod0_mu14 trigger'),
    ('Cut 13 m_ll > 20 GeV'                ,'Cut 13 Lepton Invariant Mass M_ll>20 GeV'),
    ('Cut 14 leading lepton pt>25 GeV'     ,'Cut 14 Leading Lepton pT>25 GeV'),
    ('Cut 15 subleading lepton pt>20 GeV'  ,'Cut 15 Sub-Leading Lepton pT>20 GeV'),
    ('Cut 16 MET>250 GeV'                  ,'Cut 16 MET>250 GeV'),
    ('Cut 17 Passed'                       ,'Cut 17 Passed')
]

#------------------------------------------------------------------------------#
def main ():
    """ Main Function """
    global args
    global cuts_to_compare
    files = [args.file1, args.file2]

    #--------------------------------------------------------------------------#
    # Get input files
    if not all([os.path.exists(x) for x in files]):
        print 'One of the input files could not be found.'
        sys.exit()
    print 'Importing files ...',
    cutflows = [json.load(open(x,'r')) for x in files]
    print 'Done'

    #--------------------------------------------------------------------------#
    # Get cutflow information
    cuts = [sorted(cutflow.keys()) for cutflow in cutflows]
    input_evts = [set(cutflow[cut[0]]) for cutflow, cut in zip(cutflows, cuts)]
    shared_input_evts = input_evts[0] & input_evts[1]
    diff_input_evts = [x - y for x, y in zip(input_evts,input_evts[::-1])]
    assert len(shared_input_evts), "No shared events"
    uniq_input_evts = [x - y for x, y in zip(input_evts,input_evts[::-1])]

    passed_evts = [set(cutflow[cut[-1]]) for cutflow, cut in zip(cutflows, cuts)]
    shared_evts = passed_evts[0] & passed_evts[1]
    uniq_passed_evts = [x - y for x, y in zip(passed_evts,passed_evts[::-1])]
    uniq_passed_evts = [shared_input_evts & x for x in uniq_passed_evts]

    #--------------------------------------------------------------------------#
    # Get cutflow of shared events
    file_names = [x.split('/')[-1].split('.')[0] for x in files]
    shared_cutflows = [get_cutflow(shared_input_evts, cutflow) for cutflow in cutflows]
    shared_evts_rm_per_cut = defaultdict(dict)
    uniq_shared_evts_rm_per_cut = defaultdict(dict)
    shared_evts_rm_both_cut = {}
    previous_events = [set(cutflow[cut[0]]) for cutflow, cut in zip(shared_cutflows, cuts)]
    if shared_cutflows[0].keys() == shared_cutflows[1].keys():
        cuts_to_compare = [(x, x) for x in sorted(shared_cutflows[0].keys())] 
    for cuts_to_cf in cuts_to_compare:
        flip = any(x not in y for x, y in zip(cuts_to_cf, shared_cutflows))
        cut_pair = cuts_to_cf[::-1] if flip else cuts_to_cf
        if any(x not in y for x, y in zip(cut_pair, shared_cutflows)): continue
        events = [set(cutflow[cut]) for cutflow, cut in zip(shared_cutflows, cut_pair)]
        assert all([evts.issubset(previous_evts) for evts, previous_evts in zip(events, previous_events)])
        rm_evts = [previous_evts - evts for evts, previous_evts in zip(events, previous_events)]
        for ii in range(2):
            uniq_rm_evts = rm_evts[ii] - rm_evts[-ii-1]
            name = file_names[ii]
            cut = cut_pair[ii] if cut_pair[ii] in shared_cutflows[ii] else cut_pair[-ii-1]
            uniq_shared_evts_rm_per_cut[name][cut] = list(uniq_rm_evts & previous_events[-ii-1])
            shared_evts_rm_per_cut[name][cut] = uniq_rm_evts - previous_events[-ii-1]
        shared_evts_rm_both_cut[cut_pair[0]] = rm_evts[0] & rm_evts[1]
        previous_events = events

    #--------------------------------------------------------------------------#
    # Get cutflow for diff events
    diff_evts_rm_per_cut = defaultdict(dict)
    diff_cutflows = [get_cutflow(ev, cf) for ev, cf in zip(diff_input_evts,cutflows)]
    for ii in range(2):
        name = file_names[ii]
        for cut, events in diff_cutflows[ii].iteritems():
            diff_evts_rm_per_cut[name][cut] = len(events)

    #--------------------------------------------------------------------------#
    # Get cutflow of unique evts in opposite cutflow
    uniq_evts_cutflows = {}
    uniq_evts_rm_per_cut = {}
    file_names = [x.split('/')[-1].split('.')[0] for x in files]
    for iii in range(2):
        cutflow = get_cutflow(uniq_passed_evts[iii], shared_cutflows[-1-iii])
        uniq_evts_cutflows[file_names[iii]] = cutflow

        # Get list of events that are removed by each cut
        dict_name = '%s passed events run in %s cutflow'%(file_names[iii],file_names[-1-iii])
        uniq_evts_rm_per_cut[dict_name] = {}
        sorted_cuts = sorted(cutflow.keys())
        previous_evts = set(cutflow[sorted_cuts[0]])
        for cut in sorted_cuts:
            evts = set(cutflow[cut])
            assert evts.issubset(previous_evts)
            rm_evts = previous_evts - evts
            uniq_evts_rm_per_cut[dict_name]['removed by %s'%cut] = list(rm_evts)
            previous_evts = set(evts)

    #--------------------------------------------------------------------------#
    #--------------------------------------------------------------------------#
    # Write to output file
    #--------------------------------------------------------------------------#
    #--------------------------------------------------------------------------#

    #--------------------------------------------------------------------------#
    # Get Output name
    if args.output:
        output_name = args.output.split('.')[0] + '.json'
    else:
        output_name = 'CutflowCompare_{0}_{1}.json'.format(
                file_names[0], file_names[1])
    ofile = open(output_name.replace('json','txt'),'w')
    json_ofile =  open(output_name,'w')

    #--------------------------------------------------------------------------#
    # Make Output json file
    json_ofile.write(json.dumps(uniq_evts_rm_per_cut, sort_keys=True, indent=4))
    json_ofile.write(json.dumps(uniq_shared_evts_rm_per_cut, sort_keys=True, indent=4))
    json_ofile.close()
    print 'Output written to %s'%(output_name)

    #--------------------------------------------------------------------------#
    # Preface for events cutflow

    string  = '='*80+'\n'
    string += '='*80+'\n'
    string += '{0:^80}\n'.format('CUTFLOW COMPARISON')
    string += '='*80+'\n'
    string += '='*80+'\n\n'

    string += '='*80+'\n'
    string += 'Input event summary:\n'
    total_evts = float(len(input_evts[0].union(input_evts[1])))
    n_shared = len(shared_input_evts)
    p_shared = n_shared/total_evts*100
    n_uniq = [len(x) for x in uniq_input_evts]
    p_uniq = [x/total_evts*100 for x in n_uniq]
    string += '\tTotal events: %.f\n'%total_evts
    string += '\t\tShared events: %d (%.2f%%)\n'%(n_shared, p_shared)
    string += '\t\tUnique %s events: %d (%.2f%%)\n'%(
            file_names[0], n_uniq[0], p_uniq[0])
    string += '\t\tUnique %s events: %d (%.2f%%)\n'%(
            file_names[1], n_uniq[1], p_uniq[1])
    #--------------------------------------------------------------------------#
    # Diff events cutflow
    string += '='*80+'\n'
    string += 'Cutflow of events only in one framework'
    for name, cutflow in diff_evts_rm_per_cut.iteritems():
        string += '\n%s Cutflow\n'%name
        string += '\t%20s : %10s\n'%('Cut','Event Yields')
        string += '\t'+'-'*21+'|'+'-'*10+'\n'
        for cut in sorted(cutflow.iterkeys()):
            n_events = cutflow[cut]
            string += '\t%20s : %10d\n'%(cut,n_events)
    string += '='*80+'\n'

    #--------------------------------------------------------------------------#
    # Shared events cutflow
    string += '='*80+'\n'
    string += 'Cutflow of events in both frameworks'
    for iii in range(2):
        name = file_names[iii]
        string += '\n%s Cutflow\n'%name
        string += '\t%20s : %10s\n'%('Cut','Event Yields')
        string += '\t'+'-'*21+'|'+'-'*10+'\n'
        for cut in sorted(shared_cutflows[iii].iterkeys()):
            events = set(shared_cutflows[iii][cut])
            n_events = len(events.intersection(shared_input_evts))
            string += '\t%20s : %10d\n'%(cut,n_events)
    string += '='*80+'\n'

    #--------------------------------------------------------------------------#
    # Print out extra cutflow information on which events get removed
    # by each cut specified in cuts_to_compare. Seperate removed events into
    # those removed by both cuts (shared), those remove by one cut but not
    # the other (diff), and those removed in one cut but already removed by
    # the other (other)
    if shared_evts_rm_both_cut:
        string += '\n'
        string += "Removed events Cutflow\n"
        string += "%s) diff -- other  -- shared -- other -- diff (%s\n"%(
            file_names[0],file_names[1])
    for cuts in cuts_to_compare:
        flip = any(x not in y for x, y in zip(cuts, shared_cutflows))
        cuta, cutb = cuts[::-1] if flip else cuts
        if cuta not in shared_cutflows[0] or cutb not in shared_cutflows[1]: continue
        n_evt1 = len(uniq_shared_evts_rm_per_cut[file_names[0]][cuta])
        n_evt2 = len(shared_evts_rm_per_cut[file_names[0]][cuta])
        n_shared = len(shared_evts_rm_both_cut[cuta])
        n_evt3 = len(shared_evts_rm_per_cut[file_names[1]][cutb])
        n_evt4 = len(uniq_shared_evts_rm_per_cut[file_names[1]][cutb])
        string += "%5d -- %5d -- %5d -- %5d -- %5d (%s/%s)\n"%(
            n_evt1, n_evt2, n_shared, n_evt3, n_evt4, cuta, cutb)

    #--------------------------------------------------------------------------#
    # Summary of events passing both cutflows
    string += '='*80+'\n'
    string += 'Passed event summary:\n'
    total_evts = float(len((passed_evts[0].union(passed_evts[1])).intersection(shared_input_evts)))
    if total_evts:
        n_shared = len(shared_evts)
        print "A few events passing both cutflows: ", list(shared_evts)[:10]
        p_shared = n_shared/total_evts*100
        n_uniq = [len(x) for x in uniq_passed_evts]
        p_uniq = [x/total_evts*100 for x in n_uniq]
        string += '\tTotal events: %.f\n'%total_evts
        string += '\t\tShared events: %d (%.2f%%)\n'%(n_shared, p_shared)
        string += '\t\tUnique %s events: %d (%.2f%%)\n'%(
                file_names[0], n_uniq[0], p_uniq[0])
        string += '\t\tUnique %s events: %d (%.2f%%)\n'%(
                file_names[1], n_uniq[1], p_uniq[1])

        string += '\n'
    else:
        string += 'No events passed through both cutflows entirely\n'

    #--------------------------------------------------------------------------#
    # Unique events cutflow
    string += '='*80+'\n'
    string += 'Unique passed events cutflow:\n'
    string += ' - Unique passed events are the events that only passed through \n' \
              '   one cutflow entirely while failing at least one cut \n' \
              '   in the other cutflow. The unique events are required \n' \
              '   to be part of the input events shared by both cutflows. \n'\
              '   The cutflows below take the unique events that \n'\
              '   passed one cutflow and run them through the other to \n'\
              '   determine where the events were cut. Therefore, the \n'\
              '   unique passed event cutflows should all end with zero events \n'
    for iii in range(2):
        name = file_names[iii]
        other_name = file_names[-1-iii]
        string += '\nUnique passed events from %s run through the %s cutflow \n'%(
                name, other_name)
        string += '\t%20s : %10s\n'%('Cut','Event Yields')
        string += '\t'+'-'*21+'|'+'-'*10+'\n'
        for cut in sorted(uniq_evts_cutflows[name].iterkeys()):
            n_events = len(uniq_evts_cutflows[name][cut])
            string += '\t%20s : %10d\n'%(cut,n_events)
    string += '='*80+'\n'
    ofile.write(string)
    ofile.close()
    print 'Output written to %s'%(output_name.replace('.json','.txt'))

#------------------------------------------------------------------------------#
# Functions

def get_cutflow(events, cutflow):
    """ Determine cutflow map for the given set of input events

        Args:
            events (set): Set of events to check against cutflow
            cutflow (map): Map from cuts to list of events passing those cuts

        Return:
            map: Map from cuts to list of input events passing those cuts
    """
    new_cutflow = {}
    for cut, passed_events in cutflow.iteritems():
        new_cutflow[cut] = list(set(passed_events).intersection(events))
    return new_cutflow

#------------------------------------------------------------------------------#
if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(
                description=__doc__,
                formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('file1', help='First .json cutflow',
                            default='AA_cutflow.json')
        parser.add_argument('file2', help='Second .json cutflow',
                            default='MB_cutflow.json')
        parser.add_argument('-o', '--output',
                            help='output file name')
        parser.add_argument('-v', '--verbose',
                            action='store_true', default=False,
                            help='verbose output')
        args = parser.parse_args()
        if args.verbose:
            path = str(os.path.abspath(__file__))
            print '>>>>>>>'
            print 'Running %s...'%path
            print time.asctime()
        main()
        if args.verbose:
            print time.asctime()
            time = (time.time() - start_time)
            print 'TOTAL TIME: %fs'%time
            print '<<<<<<'
        sys.exit(0)
    except KeyboardInterrupt, e: # Ctrl-C
        raise e
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
