#!/usr/bin/env python
#------------------------------------------------------------------------------#
"""
== cutflow_txt_to_json ==
Take the output of STCutflowLooper and convert it to .json file mapping
each cut in the cutflow to the events that pass that cut

EXAMPLES:
    python cutflow_txt_to_json.py cutflow.log

Author: Alex Armstrong <alarmstr@cern.ch>
Copyright: (C) Jan 11, 2018; University of California, Irvine 
"""

import sys, os, traceback, argparse
import time
import json
from collections import namedtuple

#------------------------------------------------------------------------------#
# Module-level variables
CutflowEvent = namedtuple('CutflowEvent','number cut') 
Cutflow = [
    # Names should match what is found in input .txt file
    'Cut 00 Read in',
    'Cut 01 No detector errors',
    'Cut 02 Has a good vertex',
    'Cut 03 Passed trigger',
    'Cut 04 2+ baseline leptons before OR',
    'Cut 05 Passed cleaning cuts',
    'Cut 06 2 base leptons after OR',
    'Cut 07 Opposite sign',
    'Cut 08 Isolated leptons',
    'Cut 09 2 signal leptons',
    'Cut 10 t,W,Z,H,VV truth origin',
    'Cut 11 eta < 2.47 (2.4) ele (mu)',
    'Cut 12 HLT_e17_lhloose_nod0_mu14',
    'Cut 13 m_ll > 20 GeV',
    'Cut 14 leading lepton pt>25 GeV',
    'Cut 15 subleading lepton pt>20 GeV',
    'Cut 16 MET>250 GeV',
    'Cut 17 Passed'
    ]
#------------------------------------------------------------------------------#
def main ():
    """ Main Function """
    global args
    with open(args.txt_file,'r') as ifile:
        if args.verbose: print 'Running over', os.path.abspath(ifile.name)
        cutflow_events = get_cutflow_events(ifile)

        cut_to_events_map = {}
        for cut in Cutflow:
            cut_to_events_map[cut] = get_passed_events(cutflow_events, cut)
            if args.verbose:
                n_passed_events = len(cut_to_events_map[cut])
                print '\t{0}: {1}'.format(cut, n_passed_events)
        json_str = json.dumps(cut_to_events_map, sort_keys=True, indent=4)

    output_name = args.output if args.output else '%s.json'%ifile.name[:-4]
    ofile = open(output_name,'w')
    ofile.write(json_str)
    ofile.close()
    print 'Output file written to ', output_name
        
#------------------------------------------------------------------------------#
# Functions

def get_cutflow_events(ifile):
    """ 
    Get list of events and there cutflow status from .txt file 

    Extract from the input .txt file all events and either the cut
    they didn't pass or that they passed all cuts. 
    *Delicate Formatting*: The text file is assumed to be formated like: 

    'CUTFLOW : Cut ## <name> : Event <#> : Info - <optional info>'

    Args:
        ifile (file): Input .txt file

    Returns:
        list[CutflowEvent]  
    """
    cutflow_events = []
    for line in ifile:
        if 'CUTFLOW' not in line:
            continue

        line_parts = [x.strip() for x in line.strip().split(':')]
        try:
            cut_name = line_parts[1]
            event_number = int(line_parts[2].split()[1])
        except ValueError: 
            print "Having trouble with this line:"
            print "\tLine:",line.strip()
            print "\tLine parts:",line_parts
            sys.exit()

        event = CutflowEvent(number=event_number,cut=cut_name)
        cutflow_events.append(event)

    return cutflow_events
        
def get_passed_events(event_info, cut):
    """ 
    Get list of all events passing the given cut

    *Delicate Formatting*: The text file is assumed to be formated like: 
    Cut ## <name>
    Also assumes there is a largest cut number for events that passed the cutflow

    Args:
        event_info (list[CutflowEvent])
        cut (str): name of cut in cutflow
    
    Return:
        list: Event numbers passing the given cut    
    """
    
    cut_check_number = int(cut.split()[1].strip())
    passed_events = []
    for event in event_info:
        failed_cut_number = int(event.cut.split()[1].strip())
        if failed_cut_number > cut_check_number:
            passed_events.append(event.number)
        elif event.cut == Cutflow[-1]:
            passed_events.append(event.number)
            
    return passed_events


#------------------------------------------------------------------------------#
if __name__ == '__main__':
    try:
        start_time = time.time()
        parser = argparse.ArgumentParser(
                description=__doc__,
                formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('txt_file',
                            help='path to txt file of LFVcf output')
        parser.add_argument('-o','--output',
                            help='path to / name of output .json file')
        parser.add_argument('-v', '--verbose', 
                            action='store_true', default=False, 
                            help='verbose output')
        args = parser.parse_args()

        if args.verbose: 
            print '\n'
            print 'Running {0}...'.format(os.path.basename(__file__))
            print time.asctime()
        main()
        if args.verbose: 
            print time.asctime()
            time = (time.time() - start_time)
            print 'TOTAL TIME: %fs'%time,
            print '\n'
        sys.exit(0)
    except KeyboardInterrupt, e: # Ctrl-C
        raise e
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        print 'ERROR, UNEXPECTED EXCEPTION'
        print str(e)
        traceback.print_exc()
        os._exit(1)
